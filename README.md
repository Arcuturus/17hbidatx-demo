# README #

##Getting started with Git

###First steps

*NOTE*: Do this after you have created an account on www.bitbucket.com with your school email
*WINDOWS USERS*: Download Git SCM (Git Bash) from https://git-scm.com/downloads

Run the command in git bash (or your favorite terminal) to see if Git is properly installed

```
git --version
```

Run git config to properly setup git on your system locally

```
git config --global user.name "username"  <-- whatever username you prefer
git config --global user.email "email"    <-- use school email
```

Create a folder that you wish to contain this "project" either manually or by commands. We
recommend creating a folder using the git-bash or your favorite terminal. This way you can
get your hand dirty with linux.


cd - change directory

```
cd someplace/someotherplace/yourfolder
```

pwd - see where you are
```
pwd
```

mkdir - create directory (in current folder)

```
mkdir <foldername>
mkdir navigate/to/folder/<foldername>
```

e.g.

```
cd documents
pwd
mkdir workspace
cd workspace
```

Clone the git repo into your newly created folder by copying this link

```
git clone https://Arcuturus@bitbucket.org/Arcuturus/17hbidatx-demo.git
```

Or alternatively: Visit https://bitbucket.org/Arcuturus/17hbidatx-demo/overview and copy the
link by the dropdown-menu where it says HTTPS. You can also change the dropdown to be SSH, but this will require some more setup on your behalf. 

###Demonstration

Anders & Anders will run a simple demonstration hei på deg


###Frode's cheat sheet
http://folk.ntnu.no/frh/ooprog/prosjekt/BasicUseOfGitAndBitbucket.pdf

###
